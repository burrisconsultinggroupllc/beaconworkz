//
//  CoreLocationManager.m
//  beaconworkz
//
//  Created by Jonathan Burris on 4/25/14.
//  Copyright (c) 2014 Burris Consulting Group LLC. All rights reserved.
//

#import "CoreLocationManager.h"

@interface CoreLocationManager()

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLBeaconRegion *beaconRegion;

@end

@implementation CoreLocationManager

@synthesize locationManager = _locationManager;
@synthesize beaconRegion = _beaconRegion;
@synthesize currentBeacon = _currentBeacon;

+(CoreLocationManager*)sharedInstance {
    
    static CoreLocationManager *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [[self alloc] init];
        
    });
    
    return sharedInstance;
    
}

-(id)init {
    
    if (self = [super init]) {
        
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        
        _beaconRegion = [self getBeaconRegion];
        
    }
    
    return self;
    
}

- (CLBeaconRegion*)getBeaconRegion {
    
    if (!self.beaconRegion) {
        
        NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:kBeaconRegionProximityUUID];
        
        _beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:kBeaconRegionIdentifier];
        _beaconRegion.notifyOnEntry = YES;
        _beaconRegion.notifyOnExit = YES;
        _beaconRegion.notifyEntryStateOnDisplay = YES;
        
    }
    
    return self.beaconRegion;
    
}

- (void)startMonitoringForRegion {
    
    if (![self locationServicesEnabled])
        return;
    
    [self.locationManager startMonitoringForRegion:self.beaconRegion];
    [self.locationManager requestStateForRegion:self.beaconRegion];
    
}

- (void)stopMonitoringForRegion {
    
    [self.locationManager stopRangingBeaconsInRegion:self.beaconRegion];
    [self.locationManager stopMonitoringForRegion:self.beaconRegion];
    
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region {
    
    if (![region.identifier isEqualToString:kBeaconRegionIdentifier])
        return;
    
    if (state == CLRegionStateInside) {

        [self.locationManager startRangingBeaconsInRegion:self.beaconRegion];
        
    } else if (state == CLRegionStateOutside) {
        
        [self.locationManager stopRangingBeaconsInRegion:self.beaconRegion];
        
    }
    
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    
    if (![region.identifier isEqualToString:kBeaconRegionIdentifier])
        return;
    
    [manager startRangingBeaconsInRegion:self.beaconRegion];
    
    [self logMessage:BWLogMessageRegionEntered withBeacon:nil];
    
    [self postLocalNotification:BWLocalNotificationMessageRegionEnter];
    
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    
    if (![region.identifier isEqualToString:kBeaconRegionIdentifier])
        return;
    
    [manager stopRangingBeaconsInRegion:self.beaconRegion];
    
    [self logMessage:BWLogMessageRegionExited withBeacon:nil];
    
    [self postLocalNotification:BWLocalNotificationMessageRegionExit];
    
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    
    if (![region.identifier isEqualToString:kBeaconRegionIdentifier])
        return;
    
    CLBeacon *nearestBeacon = [beacons firstObject];
    
    if (!nearestBeacon) {
        return;
    }
    
    if (self.currentBeacon.major == nearestBeacon.major && self.currentBeacon.minor == nearestBeacon.minor) {
        
        self.currentBeacon = nearestBeacon;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCurrentBeaconUpdated object:self];
        
    } else {
        
        self.currentBeacon = nearestBeacon;
        
        [self logMessage:BWLogMessageBeaconChanged withBeacon:self.currentBeacon];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCurrentBeaconChanged object:self];
    }
    
}

- (BOOL)locationServicesEnabled {
    
    BOOL enabled = NO;
    
    if ((![CLLocationManager locationServicesEnabled])
        || ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted)
        || ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)) {
            
        NSLog(@"Location Services is Not Enabled");
        
        enabled = NO;
        
    } else {
        NSLog(@"Location Services is Enabled");
        
        enabled = YES;
    }
    
    return enabled;
    
}

- (void)postLocalNotification:(BWLocalNotificationMessage)localNotificationMessage {
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.userInfo = @{@"identifier": self.beaconRegion.identifier};
    notification.soundName = @"Default";
    
    if (localNotificationMessage == BWLocalNotificationMessageRegionEnter) {
        notification.alertBody = @"You have entered our beacon region";
    } else if (localNotificationMessage == BWLocalNotificationMessageRegionExit) {
        notification.alertBody = @"You have left our beacon region. Thanks for visiting.";
    }
    
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    
}

- (void)logMessage:(BWLogMessage)logMessage withBeacon:(CLBeacon*)beacon {
    
    NSString *logString;
    
    if (logMessage == BWLogMessageRegionEntered) {
        logString = @"User entered the store";
    } else if (logMessage == BWLogMessageRegionExited) {
        logString = @"User exited the store";
    } else if (logMessage == BWLogMessageBeaconChanged) {
        logString = [NSString stringWithFormat:@"User approached beacon: Major (%lu), Minor (%lu), Proximity (%@)", (long)[beacon.major integerValue], (long)[beacon.minor integerValue], [self stringForProximity:beacon.proximity] ];
    }
    
    NSLog(@"%@", logString);
    
    ///TODO: upload logged info to server
    
}

- (NSString *)stringForProximity:(CLProximity)proximity {
    switch (proximity) {
        case CLProximityUnknown:    return @"Unknown";
        case CLProximityFar:        return @"Far";
        case CLProximityNear:       return @"Near";
        case CLProximityImmediate:  return @"Immediate";
        default:
            return nil;
    }
}

@end
