//
//  FlipsideViewController.h
//  beaconworkz
//
//  Created by Jonathan Burris on 4/21/14.
//  Copyright (c) 2014 Burris Consulting Group LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FlipsideViewController;

@protocol FlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller;
@end

@interface FlipsideViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) id <FlipsideViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)done:(id)sender;

@end
