//
//  AppDelegate.h
//  beaconworkz
//
//  Created by Jonathan Burris on 4/21/14.
//  Copyright (c) 2014 Burris Consulting Group LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
