//
//  Product.h
//  beaconworkz
//
//  Created by Jonathan Burris on 4/22/14.
//  Copyright (c) 2014 Burris Consulting Group LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property (nonatomic, strong) NSNumber* major;
@property (nonatomic, strong) NSNumber* minor;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* imageName;
@property (nonatomic, strong) NSString* detailText;
@property (nonatomic, strong) NSNumber* price;
@property (nonatomic, strong) NSNumber* salePrice;
@property (nonatomic, assign, getter = isActivePromotion) BOOL activePromotion;

- (id)initWithDictionary:(NSDictionary*)dict;

@end
