//
//  DataManager.m
//  beaconworkz
//
//  Created by Jonathan Burris on 4/22/14.
//  Copyright (c) 2014 Burris Consulting Group LLC. All rights reserved.
//

#import "DataManager.h"
#import "Product.h"

@interface DataManager()
    
    @property (nonatomic, strong) NSMutableArray *products;

@end

@implementation DataManager

@synthesize products = _products;

+ (DataManager*)sharedInstance {
    
    static DataManager* sharedInstance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
    
}

- (id)init {
    
    if (self = [super init]) {
        
        ///TODO: perform any initialization here
    }
    
    return self;
    
}

- (NSMutableArray*)getProducts {
    
    if (!_products) {
        
        _products = [[NSMutableArray alloc] init];
        
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"beaconDataRepository" ofType:@"plist"];
        
        NSArray *tmp = [NSArray arrayWithContentsOfFile:filePath];
        
        for (int i = 0; i < [tmp count]; i++) {
            NSDictionary *dict = [tmp objectAtIndex:i];
            
            Product *thisProduct = [[Product alloc] initWithDictionary:dict];
            [_products addObject:thisProduct];
        }
        
    }
    
    return _products;
    
}

@end
