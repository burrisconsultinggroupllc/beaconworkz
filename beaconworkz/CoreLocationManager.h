//
//  CoreLocationManager.h
//  beaconworkz
//
//  Created by Jonathan Burris on 4/25/14.
//  Copyright (c) 2014 Burris Consulting Group LLC. All rights reserved.
//

#define kBeaconRegionProximityUUID  @"B9407F30-F5F8-466E-AFF9-25556B57FE6D"
#define kBeaconRegionIdentifier     @"com.burrisconsultinggroup.beaconworkz"

#define kNotificationCurrentBeaconChanged  @"NotificationCurrentBeaconChanged"
#define kNotificationCurrentBeaconUpdated  @"NotificationCurrentBeaconUpdated"

typedef NS_ENUM(NSInteger, BWLocalNotificationMessage) {
    BWLocalNotificationMessageRegionEnter,
    BWLocalNotificationMessageRegionExit
};

typedef NS_ENUM(NSInteger, BWLogMessage) {
    BWLogMessageRegionEntered,
    BWLogMessageRegionExited,
    BWLogMessageBeaconChanged
};

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface CoreLocationManager : NSObject <CLLocationManagerDelegate>

@property (nonatomic, strong) CLBeacon* currentBeacon;

+ (CoreLocationManager*)sharedInstance;

- (void)startMonitoringForRegion;
- (void)stopMonitoringForRegion;

- (BOOL)locationServicesEnabled;

- (void)logMessage:(BWLogMessage)logMessage withBeacon:(CLBeacon*)beacon;
- (NSString *)stringForProximity:(CLProximity)proximity;

@end
