//
//  FlipsideViewController.m
//  beaconworkz
//
//  Created by Jonathan Burris on 4/21/14.
//  Copyright (c) 2014 Burris Consulting Group LLC. All rights reserved.
//

#import "FlipsideViewController.h"
#import "CoreLocationManager.h"

@interface FlipsideViewController ()

@property (nonatomic, strong) CLBeacon *currentBeacon;

@end

@implementation FlipsideViewController

@synthesize currentBeacon = _currentBeacon;

- (void)awakeFromNib
{
    self.preferredContentSize = CGSizeMake(320.0, 480.0);
    [super awakeFromNib];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData:) name:kNotificationCurrentBeaconChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData:) name:kNotificationCurrentBeaconUpdated object:nil];
        
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationCurrentBeaconChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationCurrentBeaconUpdated object:nil];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self refreshData:self];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)done:(id)sender
{
    [self.delegate flipsideViewControllerDidFinish:self];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];
    }
    
    if ([indexPath row] == 0) {
        cell.textLabel.text = @"UUID";
        cell.detailTextLabel.text = [self.currentBeacon.proximityUUID UUIDString];
    } else if ([indexPath row] == 1) {
        cell.textLabel.text = @"Major";
        cell.detailTextLabel.text = [self.currentBeacon.major stringValue];
    } else if ([indexPath row] == 2) {
        cell.textLabel.text = @"Minor";
        cell.detailTextLabel.text = [self.currentBeacon.minor stringValue];
    } else if ([indexPath row] == 3) {
        cell.textLabel.text = @"Proximity";
        cell.detailTextLabel.text = [[CoreLocationManager sharedInstance] stringForProximity:self.currentBeacon.proximity];
    } else if ([indexPath row] == 4) {
        cell.textLabel.text = @"Accuracy";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%f", self.currentBeacon.accuracy];
    } else if ([indexPath row] == 5) {
        cell.textLabel.text = @"RSSI";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%lu", (long)self.currentBeacon.rssi];
    }
    
    return cell;
    
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)refreshData:(id)sender {
    
    _currentBeacon = [[CoreLocationManager sharedInstance] currentBeacon];
    
    [self.tableView reloadData];
    
}

@end
