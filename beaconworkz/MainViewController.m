//
//  MainViewController.m
//  beaconworkz
//
//  Created by Jonathan Burris on 4/21/14.
//  Copyright (c) 2014 Burris Consulting Group LLC. All rights reserved.
//

#import "MainViewController.h"
#import "CoreLocationManager.h"
#import "DataManager.h"
#import "Product.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeBeacon:) name:kNotificationCurrentBeaconChanged object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationCurrentBeaconChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationCurrentBeaconUpdated object:nil];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Flipside View Controller

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.flipsidePopoverController dismissPopoverAnimated:YES];
    }
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.flipsidePopoverController = nil;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showAlternate"]) {
        [[segue destinationViewController] setDelegate:self];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            UIPopoverController *popoverController = [(UIStoryboardPopoverSegue *)segue popoverController];
            self.flipsidePopoverController = popoverController;
            popoverController.delegate = self;
        }
    }
}

- (IBAction)togglePopover:(id)sender
{
    if (self.flipsidePopoverController) {
        [self.flipsidePopoverController dismissPopoverAnimated:YES];
        self.flipsidePopoverController = nil;
    } else {
        [self performSegueWithIdentifier:@"showAlternate" sender:sender];
    }
}

- (void)changeBeacon:(id)sender {
    
    CLBeacon *currentBeacon = [[CoreLocationManager sharedInstance] currentBeacon];
    
    NSArray *products = [[DataManager sharedInstance] getProducts];
    
    if ([currentBeacon.minor integerValue] >= [products count])
        return;
    
    Product *currentProduct = [products objectAtIndex:[currentBeacon.minor integerValue]];
    
    if (!currentProduct)
        return;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.title = currentProduct.title;
    } else {
        self.titleLabel.text = currentProduct.title;
    }
    
    self.productImage.image = [UIImage imageNamed:currentProduct.imageName];
    self.detailTextView.text = currentProduct.detailText;
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    self.priceLabel.text = (currentProduct.isActivePromotion) ? [formatter stringFromNumber:currentProduct.price] : [formatter stringFromNumber:currentProduct.salePrice];
    
    self.promotionLabel.text = (currentProduct.isActivePromotion) ? @"On Sale Now" : @"Everyday Low Price";
    
}

@end
