//
//  DataManager.h
//  beaconworkz
//
//  Created by Jonathan Burris on 4/22/14.
//  Copyright (c) 2014 Burris Consulting Group LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

+ (DataManager*)sharedInstance;

- (NSMutableArray*)getProducts;

@end
