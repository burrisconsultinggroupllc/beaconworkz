//
//  Product.m
//  beaconworkz
//
//  Created by Jonathan Burris on 4/22/14.
//  Copyright (c) 2014 Burris Consulting Group LLC. All rights reserved.
//

#import "Product.h"

@implementation Product

- (id)initWithDictionary:(NSDictionary*)dict {
    
    if (self = [super init]) {
        
        self.major = [dict objectForKey:@"major"];
        self.minor = [dict objectForKey:@"minor"];
        self.title = [dict objectForKey:@"title"];
        self.imageName = [dict objectForKey:@"imageName"];
        self.detailText = [dict objectForKey:@"detailText"];
        self.price = [dict objectForKey:@"price"];
        self.salePrice = [dict objectForKey:@"salePrice"];
        self.activePromotion = [[dict objectForKey:@"activePromotion"] boolValue];
        
    }
    
    return self;
    
}

@end
