//
//  beaconworkzTests.m
//  beaconworkzTests
//
//  Created by Jonathan Burris on 4/21/14.
//  Copyright (c) 2014 Burris Consulting Group LLC. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DataManager.h"
#import "CoreLocationManager.h"
#import "Product.h"

@interface beaconworkzTests : XCTestCase

@property (nonatomic, strong) NSArray *products;

@end

@implementation beaconworkzTests

@synthesize products = _products;

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.

    _products = [[DataManager sharedInstance] getProducts];

}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    _products = nil;
}

- (void)testDataManager
{
    NSInteger expected = 3;
    NSInteger actual = [self.products count];
    
    XCTAssertEqual(actual, expected, @"Expected '%lu', but received '%lu' for \"%s\"", actual, expected, __PRETTY_FUNCTION__);
}

- (void)testCoreLocationManagerLogMessage {
    
    XCTAssertNoThrow([[CoreLocationManager sharedInstance] logMessage:BWLogMessageRegionEntered withBeacon:nil]
                     , @"Expected No Throw for \"%s\"", __PRETTY_FUNCTION__);
    
}

- (void)testCoreLocationManagerStringForProximity {
    
    NSString *expected = @"Immediate";
    NSString *actual = [[CoreLocationManager sharedInstance] stringForProximity:CLProximityImmediate];
    XCTAssert([actual isEqualToString:expected], @"Expected '%@', but received '%@' for \"%s\"", actual, expected, __PRETTY_FUNCTION__);
    
}

- (void)testLocationServicesEnabled {
    
    BOOL actual = [[CoreLocationManager sharedInstance] locationServicesEnabled];
    
    XCTAssertTrue(actual, @"Expected 'YES', but received 'NO' for \"%s\"", __PRETTY_FUNCTION__);
    
}

- (void)testProductIsActivePromotionProperty {
    
    Product *firstProduct = [self.products objectAtIndex:0];
    Product *lastProduct = [self.products objectAtIndex:2];
    
    XCTAssertNotEqual(firstProduct.isActivePromotion, lastProduct.isActivePromotion, @"Expected 'NO', but received 'YES' for \"%s\"", __PRETTY_FUNCTION__);
    
    
}

@end
